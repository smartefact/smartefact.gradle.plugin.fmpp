[![Latest release](https://gitlab.com/smartefact/smartefact.gradle.plugin.fmpp/-/badges/release.svg?key_text=Latest%20release&key_width=100)](https://gitlab.com/smartefact/smartefact.gradle.plugin.fmpp/-/releases)
[![Pipeline status](https://gitlab.com/smartefact/smartefact.gradle.plugin.fmpp/badges/main/pipeline.svg?key_text=Pipeline%20status&key_width=100)](https://gitlab.com/smartefact/smartefact.gradle.plugin.fmpp/-/commits/main)

# smartefact.gradle.plugin.fmpp

[Gradle](https://gradle.org/) plugin to use [FMPP](https://fmpp.sourceforge.net/).

This plugin is very helpful to generate

- source files,
- documentation,
- static websites,
- etc.

## Usage

Users should be familiar with the basic FMPP and FreeMarker concepts.
The [FMPP Manual](https://fmpp.sourceforge.net/manual.html)
and [FreeMarker Manual](https://freemarker.apache.org/docs/index.html) are helpful resources.

### Using the Smartefact Maven repository

`settings.gradle.kts`

```kotlin
pluginManagement {
    repositories {
        // Gradle Portal
        gradlePluginPortal()
        // Smartefact Maven repository
        maven("https://gitlab.com/api/v4/groups/smartefact/-/packages/maven")
        // Other repositories...
    }
}
```

### Applying the FMPP plugin

`build.gradle.kts`

```kotlin
plugins {
    id("smartefact.fmpp") version "0.3.0"
    // Other plugins...
}
```

### Configuring the FMPP extension

`build.gradle.kts`

```kotlin
fmpp {
    data.put("myName", "Laurent")
    // Other properties...
}
```

| Property                     | Description                                                                                                        | Default value |
|------------------------------|--------------------------------------------------------------------------------------------------------------------|---------------|
| `fmppVersion`                | FMPP version to use                                                                                                | `0.9.16`      |
| `sourceEncoding`             | [Source encoding](https://fmpp.sourceforge.net/settings.html#key_sourceEncoding)                                   | `UTF-8`       |
| `outputEncoding`             | [Output encoding](https://fmpp.sourceforge.net/settings.html#key_outputEncoding)                                   | `source`      |
| `caseSensitive`              | [Case sensitivity](https://fmpp.sourceforge.net/settings.html#key_caseSensitive)                                   | `true`        |
| `dontTraverseDirectories`    | Don't recursively process the directories in source files                                                          | `false`       |
| `alwaysCreateDirectories`    | Create output directories for empty source directories                                                             | `false`       |
| `removeExtensions`           | [Extensions to remove](https://fmpp.sourceforge.net/settings.html#key_removeExtensions)                            | none          |
| `replaceExtensions`          | [Mapping of extensions to replace](https://fmpp.sourceforge.net/settings.html#key_replaceExtensions)               | none          |
| `removePostfixes`            | [Postfixes to remove](https://fmpp.sourceforge.net/settings.html#key_removePostfixes)                              | none          |
| `removeFreemarkerExtensions` | [Remove standard FreeMarker extensions](https://fmpp.sourceforge.net/settings.html#key_removeFreemarkerExtensions) | `true`        |
| `locale`                     | [Locale](https://fmpp.sourceforge.net/settings.html#key_locale)                                                    | `en_US`       |
| `timeZone`                   | [Time zone](https://fmpp.sourceforge.net/settings.html#key_timeZone)                                               | none          |
| `booleanFormat`              | [Boolean format](https://fmpp.sourceforge.net/settings.html#key_booleanFormat)                                     | none          |
| `numberFormat`               | [Number format](https://fmpp.sourceforge.net/settings.html#key_numberFormat)                                       | none          |
| `dateFormat`                 | [Date format](https://fmpp.sourceforge.net/settings.html#key_dateFormat)                                           | none          |
| `timeFormat`                 | [Time format](https://fmpp.sourceforge.net/settings.html#key_timeFormat)                                           | none          |
| `dateTimeFormat`             | [Date/time format](https://fmpp.sourceforge.net/settings.html#key_datetimeFormat)                                  | none          |
| `data`                       | Map of data available to all templates and used by all FMPP tasks                                                  | none          |
| `attributes`                 | Map of attributes used by all FMPP tasks                                                                           | none          |

### Creating FMPP tasks

The FMPP plugin does not create any task automatically.

Creating FMPP tasks is very easy, as all properties are optional.

`build.gradle.kts`

```kotlin
val processTemplates by tasks.registering(FmppTask::class) {
    sourceRoot.set("src/templates")
    exclude("**/package-info.java")
    // Other properties...
}
```

| Property                     | Description                                                                                                                                 | Default value          |
|------------------------------|---------------------------------------------------------------------------------------------------------------------------------------------|------------------------|
| `sourceRoot`                 | [Source root](https://fmpp.sourceforge.net/settings.html#key_sourceRoot)                                                                    | `src/fmpp`             |
| `outputRoot`                 | [Output root](https://fmpp.sourceforge.net/settings.html#key_outputRoot)                                                                    | `$buildDir/fmpp`       |
| `dataRoot`                   | [Data root](https://fmpp.sourceforge.net/settings.html#key_dataRoot)                                                                        | Same as `sourceRoot`   |
| `sourceEncoding`             | [Source encoding](https://fmpp.sourceforge.net/settings.html#key_sourceEncoding)                                                            | Same as FMPP extension |
| `outputEncoding`             | [Output encoding](https://fmpp.sourceforge.net/settings.html#key_outputEncoding)                                                            | Same as FMPP extension |
| `caseSensitive`              | [Case sensitivity](https://fmpp.sourceforge.net/settings.html#key_caseSensitive)                                                            | Same as FMPP extension |
| `dontTraverseDirectories`    | Don't recursively process the directories in source files                                                                                   | Same as FMPP extension |
| `alwaysCreateDirectories`    | Create output directories for empty source directories                                                                                      | Same as FMPP extension |
| `removeExtensions`           | [Extensions to remove](https://fmpp.sourceforge.net/settings.html#key_removeExtensions) (added to those of the FMPP extension)              | none                   |
| `replaceExtensions`          | [Mapping of extensions to replace](https://fmpp.sourceforge.net/settings.html#key_replaceExtensions) (added to those of the FMPP extension) | none                   |
| `removePostfixes`            | [Postfixes to remove](https://fmpp.sourceforge.net/settings.html#key_removePostfixes) (added to those of the FMPP extension)                | none                   |
| `removeFreemarkerExtensions` | [Remove standard FreeMarker extensions](https://fmpp.sourceforge.net/settings.html#key_removeFreemarkerExtensions)                          | Same as FMPP extension |
| `locale`                     | [Locale](https://fmpp.sourceforge.net/settings.html#key_locale)                                                                             | Same as FMPP extension |
| `timeZone`                   | [Time zone](https://fmpp.sourceforge.net/settings.html#key_timeZone)                                                                        | Same as FMPP extension |
| `booleanFormat`              | [Boolean format](https://fmpp.sourceforge.net/settings.html#key_booleanFormat)                                                              | Same as FMPP extension |
| `numberFormat`               | [Number format](https://fmpp.sourceforge.net/settings.html#key_numberFormat)                                                                | Same as FMPP extension |
| `dateFormat`                 | [Date format](https://fmpp.sourceforge.net/settings.html#key_dateFormat)                                                                    | Same as FMPP extension |
| `timeFormat`                 | [Time format](https://fmpp.sourceforge.net/settings.html#key_timeFormat)                                                                    | Same as FMPP extension |
| `dateTimeFormat`             | [Date/time format](https://fmpp.sourceforge.net/settings.html#key_datetimeFormat)                                                           | Same as FMPP extension |
| `data`                       | Map of data available to all templates (added to those of the FMPP extension)                                                               | none                   |
| `attributes`                 | Map of attributes used by all templates (added to those of the FMPP extension)                                                              | none                   |

| Method    | Description                                  |
|-----------|----------------------------------------------|
| `include` | Includes file patterns under the source root |
| `exclude` | Excludes file patterns under the source root |

### Configuring the FMPP classpath

The FMPP plugin creates a `fmpp` configuration.
Dependencies can be explicitly added to that configuration (e.g. a specific FreeMarker version, custom data loaders),
in which case the FMPP library must be explicitly added, too.

`build.gradle.kts`

```kotlin
dependencies {
    fmpp("com.example:example-library:1.2.3")
    fmpp("net.sourceforge.fmpp:fmpp:0.9.42")
}
```

By default, this configuration contains the FMPP library specified by `fmppVersion` in the FMPP extension.

## Changelog

See [CHANGELOG](CHANGELOG.md).

## Contributing

See [CONTRIBUTING](CONTRIBUTING.md).

## License

This project is licensed under the [Apache License 2.0](LICENSE).
