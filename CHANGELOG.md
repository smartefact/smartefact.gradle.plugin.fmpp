# Changelog

This changelog is based on [Keep a Changelog 1.0.0](https://keepachangelog.com/en/1.0.0).

This project adheres to [Semantic Versioning 2.0.0](https://semver.org/spec/v2.0.0).

## [0.3.0] - 2023-09-15

### Added

- Added load data operations in `FmppTask`
- Added load data operations in `FmppExtension`

## [0.2.0] - 2023-06-02

### Changed

- Simplify `FmppTask` by adding `include`/`exclude` methods and making the `sourceFiles` property read-only

## [0.1.0] - 2023-05-31

### Added

- Created `smartefact.fmpp` plugin
