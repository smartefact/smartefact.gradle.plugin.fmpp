/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.fmpp

import fmpp.Engine
import fmpp.tdd.DataLoader
import fmpp.tdd.EvalException
import fmpp.tdd.TddUtil

internal fun Engine.addRemoveExtensions(extensions: Iterable<String>) {
    for (extension in extensions) {
        addRemoveExtension(extension)
    }
}

internal fun Engine.addReplaceExtensions(mapping: Map<String, String>) {
    for ((oldExtension, newExtension) in mapping) {
        addReplaceExtension(oldExtension, newExtension)
    }
}

internal fun Engine.addRemovePostfixes(postfixes: Iterable<String>) {
    for (extension in postfixes) {
        addRemovePostfix(extension)
    }
}

internal fun Engine.addAttributes(attributes: Map<String, Any>) {
    for ((name, value) in attributes) {
        setAttribute(name, value)
    }
}

internal fun Engine.getDataLoader(name: String): DataLoader? =
    try {
        TddUtil.getDataLoaderInstance(this, name)
    } catch (e: EvalException) {
        null
    }
