/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.fmpp

import freemarker.log.Logger
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.attributes.Usage

/**
 * FMPP [Plugin].
 *
 * @author Laurent Pireyn
 */
open class FmppPlugin : Plugin<Project> {
    override fun apply(project: Project) {
        val fmppExtension = project.extensions.create("fmpp", FmppExtension::class.java)
        project.configurations.create(FMPP_CONFIGURATION).apply {
            description = "Libraries used by FMPP."
            isTransitive = true
            isVisible = false
            isCanBeResolved = true
            isCanBeConsumed = false
            this.attributes.attribute(Usage.USAGE_ATTRIBUTE, project.objects.named(Usage::class.java, Usage.JAVA_RUNTIME))
            defaultDependencies { dependencies ->
                dependencies.add(project.dependencies.create("net.sourceforge.fmpp:fmpp:${fmppExtension.fmppVersion.get()}"))
            }
        }
        configureFreeMarker()
    }

    companion object {
        const val FMPP_CONFIGURATION = "fmpp"
        const val DEFAULT_FMPP_VERSION = "0.9.16"

        private fun configureFreeMarker() {
            // Configure FreeMarker logging to use JUL
            // Gradle redirects JUL statements to its own logging
            // See https://docs.gradle.org/current/userguide/logging.html#sec:external_tools
            System.setProperty(Logger.SYSTEM_PROPERTY_NAME_LOGGER_LIBRARY, Logger.LIBRARY_NAME_JUL);
        }
    }
}
