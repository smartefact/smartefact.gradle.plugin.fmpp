/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.fmpp

import fmpp.Engine
import freemarker.template.Configuration
import org.gradle.api.GradleException
import org.gradle.api.logging.Logging
import org.gradle.workers.WorkAction

/**
 * FMPP [WorkAction].
 *
 * @author Laurent Pireyn
 */
internal abstract class FmppWorkAction : WorkAction<FmppWorkParameters> {
    override fun execute() {
        val engine = createEngine()
        LOGGER.info(
            "Running FMPP (sourceRoot={}, outputRoot={}, dataRoot={}, sourceFiles={})",
            engine.sourceRoot,
            engine.outputRoot,
            engine.dataRoot,
            parameters.sourceFiles
        )
        engine.process(parameters.sourceFiles.toTypedArray())
    }

    private fun createEngine(): Engine =
        // The version used for recommendedDefaults doesn't matter much, as most properties are explicitly set
        Engine(Engine.VERSION_0_9_16).apply {
            sourceRoot = parameters.sourceRoot.get().asFile
            outputRoot = parameters.outputRoot.get().asFile
            dataRoot = parameters.dataRoot.getOrNull()?.asFile
            sourceEncoding = parameters.sourceEncoding.get()
            outputEncoding = parameters.outputEncoding.get()
            caseSensitive = parameters.caseSensitive.get()
            dontTraverseDirectories = parameters.dontTraverseDirectories.get()
            alwaysCreateDirectories = parameters.alwaysCreateDirectories.get()
            addRemoveExtensions(parameters.removeExtensions.get())
            addReplaceExtensions(parameters.replaceExtensions.get())
            addRemovePostfixes(parameters.removePostfixes.get())
            removeFreemarkerExtensions = parameters.removeFreemarkerExtensions.get()
            setLocale(parameters.locale.get())
            parameters.timeZone.getOrNull()?.let { setTimeZone(it) }
            parameters.booleanFormat.getOrNull()?.let { setBooleanFormat(it) }
            parameters.numberFormat.getOrNull()?.let { numberFormat = it }
            parameters.dateFormat.getOrNull()?.let { dateFormat = it }
            parameters.timeFormat.getOrNull()?.let { timeFormat = it }
            parameters.dateTimeFormat.getOrNull()?.let { dateTimeFormat = it }
            tagSyntax = Configuration.AUTO_DETECT_TAG_SYNTAX
            interpolationSyntax = Configuration.DOLLAR_INTERPOLATION_SYNTAX
            // Attributes must be added before data, as they can influence data loaders
            addAttributes(parameters.attributes.get())
            // Load data
            for ((name, dataLoaderName, args) in parameters.loadDataOperations.get()) {
                val dataLoader = getDataLoader(dataLoaderName)
                    ?: throw GradleException("Unknown data loader: $dataLoaderName")
                val data = try {
                    dataLoader.load(this, args)
                } catch (e: Exception) {
                    throw GradleException("Error when loading data \"$name\" with data loader $dataLoaderName", e)
                }
                addData(name, data)
            }
            // Add data
            addData(parameters.data.get())
        }

    companion object {
        private val LOGGER = Logging.getLogger(FmppWorkAction::class.java)
    }
}
