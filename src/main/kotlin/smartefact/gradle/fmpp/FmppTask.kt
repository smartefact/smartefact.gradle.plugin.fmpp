/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.fmpp

import groovy.lang.Closure
import org.gradle.api.DefaultTask
import org.gradle.api.file.ConfigurableFileCollection
import org.gradle.api.file.DirectoryProperty
import org.gradle.api.file.FileTree
import org.gradle.api.file.FileTreeElement
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.provider.Provider
import org.gradle.api.specs.Spec
import org.gradle.api.tasks.*
import org.gradle.api.tasks.util.PatternSet
import org.gradle.internal.Factory
import org.gradle.workers.WorkerExecutor
import javax.inject.Inject

/**
 * FMPP task.
 *
 * Creating a `FmppTask` requires the project to have a [FmppExtension].
 *
 * @author Laurent Pireyn
 */
abstract class FmppTask : DefaultTask() {
    private val fmppExtension = project.extensions.getByType(FmppExtension::class.java)

    /**
     * Source root.
     *
     * Default: `src/fmpp`
     */
    @get:InputDirectory
    abstract val sourceRoot: DirectoryProperty
    init {
        sourceRoot.convention(project.layout.projectDirectory.dir("src/fmpp"))
    }

    @get:Inject
    internal abstract val patternSetFactory: Factory<PatternSet>

    @get:Internal
    internal val patternSet: PatternSet = checkNotNull(patternSetFactory.create())

    @get:InputFiles
    val sourceFiles: FileTree
        get() = sourceRoot.asFileTree.matching(patternSet)

    /**
     * Output root.
     *
     * Default: `$buildDir/fmpp`
     */
    @get:OutputDirectory
    abstract val outputRoot: DirectoryProperty
    init {
        outputRoot.convention(project.layout.buildDirectory.dir("fmpp"))
    }

    /**
     * Data root.
     *
     * Default: same as [sourceRoot]
     */
    @get:InputDirectory
    @get:Optional
    abstract val dataRoot: DirectoryProperty

    /**
     * Source encoding.
     *
     * Default: `UTF-8`
     */
    @get:Input
    abstract val sourceEncoding: Property<String>
    init {
        sourceEncoding.convention(fmppExtension.sourceEncoding)
    }

    /**
     * Output encoding.
     *
     * Default: `source`, i.e. same as [sourceEncoding]
     */
    @get:Input
    abstract val outputEncoding: Property<String>
    init {
        outputEncoding.convention(fmppExtension.outputEncoding)
    }

    @get:Input
    abstract val caseSensitive: Property<Boolean>
    init {
        caseSensitive.convention(fmppExtension.caseSensitive)
    }

    @get:Input
    abstract val dontTraverseDirectories: Property<Boolean>
    init {
        dontTraverseDirectories.convention(fmppExtension.dontTraverseDirectories)
    }

    @get:Input
    abstract val alwaysCreateDirectories: Property<Boolean>
    init {
        alwaysCreateDirectories.convention(fmppExtension.alwaysCreateDirectories)
    }

    @get:Internal
    abstract val removeExtensions: ListProperty<String>

    @get:Input
    internal val allRemoveExtensions: Provider<List<String>> = project.provider { fmppExtension.removeExtensions.get() + removeExtensions.get() }

    @get:Internal
    abstract val replaceExtensions: MapProperty<String, String>

    @get:Input
    internal val allReplaceExtensions: Provider<Map<String, String>> = project.provider { fmppExtension.replaceExtensions.get() + replaceExtensions.get() }

    @get:Internal
    abstract val removePostfixes: ListProperty<String>

    @get:Input
    internal val allRemovePostfixes: Provider<List<String>> = project.provider { fmppExtension.removePostfixes.get() + removePostfixes.get() }

    @get:Input
    abstract val removeFreemarkerExtensions: Property<Boolean>
    init {
        removeFreemarkerExtensions.convention(fmppExtension.removeFreemarkerExtensions)
    }

    /**
     * Locale.
     *
     * Default: `en_US`
     */
    @get:Input
    abstract val locale: Property<String>
    init {
        locale.convention(fmppExtension.locale)
    }

    @get:Input
    @get:Optional
    abstract val timeZone: Property<String>
    init {
        timeZone.convention(fmppExtension.timeZone)
    }

    @get:Input
    @get:Optional
    abstract val booleanFormat: Property<String>
    init {
        booleanFormat.convention(fmppExtension.booleanFormat)
    }

    @get:Input
    @get:Optional
    abstract val numberFormat: Property<String>
    init {
        numberFormat.convention(fmppExtension.numberFormat)
    }

    @get:Input
    @get:Optional
    abstract val dateFormat: Property<String>
    init {
        dateFormat.convention(fmppExtension.dateFormat)
    }

    @get:Input
    @get:Optional
    abstract val timeFormat: Property<String>
    init {
        timeFormat.convention(fmppExtension.timeFormat)
    }

    @get:Input
    @get:Optional
    abstract val dateTimeFormat: Property<String>
    init {
        dateTimeFormat.convention(fmppExtension.dateTimeFormat)
    }

    /**
     * Attributes.
     */
    @get:Internal
    abstract val attributes: MapProperty<String, Any>

    @get:Input
    internal val allAttributes: Provider<Map<String, Any>> = project.provider { fmppExtension.attributes.get() + attributes.get() }

    @get:Internal
    internal abstract val loadDataOperations: ListProperty<LoadDataOperation>

    /**
     * Loads data under the given [name],
     * using the given [data loader][dataLoaderName] and [args].
     */
    fun data(name: String, dataLoaderName: String, args: List<Any>) {
        loadDataOperations.add(LoadDataOperation(name, dataLoaderName, args))
    }

    /**
     * Loads data under the given [name],
     * using the given [data loader][dataLoaderName] and [args].
     */
    fun data(name: String, dataLoaderName: String, vararg args: Any) {
        data(name, dataLoaderName, args.toList())
    }

    @get:Input
    internal val allLoadDataOperations: Provider<List<LoadDataOperation>> = project.provider { fmppExtension.loadDataOperations.get() + loadDataOperations.get() }

    /**
     * Data available to all templates.
     */
    @get:Internal
    abstract val data: MapProperty<String, Any?>

    @get:Input
    internal val allData: Provider<Map<String, Any?>> = project.provider { fmppExtension.data.get() + data.get() }

    /**
     * FMPP classpath.
     *
     * Defaults to the `fmpp` configuration.
     */
    @get:InputFiles
    abstract val fmppClasspath: ConfigurableFileCollection
    init {
        fmppClasspath.setFrom(project.configurations.getByName(FmppPlugin.FMPP_CONFIGURATION))
    }

    @get:Inject
    internal abstract val workerExecutor: WorkerExecutor

    fun include(vararg includes: String) {
        patternSet.include(*includes)
    }

    fun include(includes: Iterable<String>) {
        patternSet.include(includes)
    }

    fun include(includeSpec: Spec<FileTreeElement>) {
        patternSet.include(includeSpec)
    }

    fun include(includeSpec: Closure<FileTreeElement>) {
        patternSet.include(includeSpec)
    }

    fun exclude(vararg excludes: String) {
        patternSet.exclude(*excludes)
    }

    fun exclude(excludes: Iterable<String>) {
        patternSet.exclude(excludes)
    }

    fun exclude(excludeSpec: Spec<FileTreeElement>) {
        patternSet.exclude(excludeSpec)
    }

    fun exclude(excludeSpec: Closure<FileTreeElement>) {
        patternSet.exclude(excludeSpec)
    }

    @TaskAction
    fun runFmpp() {
        val workQueue = workerExecutor.classLoaderIsolation { spec ->
            spec.classpath.from(fmppClasspath)
        }
        workQueue.submit(FmppWorkAction::class.java) { parameters ->
            parameters.sourceRoot = sourceRoot
            parameters.outputRoot = outputRoot
            parameters.dataRoot = dataRoot
            parameters.sourceFiles = sourceFiles.files
            parameters.sourceEncoding = sourceEncoding
            parameters.outputEncoding = outputEncoding
            parameters.caseSensitive = caseSensitive
            parameters.dontTraverseDirectories = dontTraverseDirectories
            parameters.alwaysCreateDirectories = alwaysCreateDirectories
            parameters.removeExtensions = allRemoveExtensions
            parameters.replaceExtensions = allReplaceExtensions
            parameters.removePostfixes = allRemovePostfixes
            parameters.removeFreemarkerExtensions = removeFreemarkerExtensions
            parameters.locale = locale
            parameters.timeZone = timeZone
            parameters.booleanFormat = booleanFormat
            parameters.numberFormat = numberFormat
            parameters.dateFormat = dateFormat
            parameters.timeFormat = timeFormat
            parameters.dateTimeFormat = dateTimeFormat
            parameters.attributes = allAttributes
            parameters.loadDataOperations = allLoadDataOperations
            parameters.data = allData
        }
    }
}
