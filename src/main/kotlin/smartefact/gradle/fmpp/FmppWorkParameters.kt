/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.fmpp

import org.gradle.api.file.Directory
import org.gradle.api.provider.Provider
import org.gradle.workers.WorkParameters
import java.io.File

/**
 * [WorkParameters] for [FmppWorkAction].
 *
 * @author Laurent Pireyn
 */
internal interface FmppWorkParameters : WorkParameters {
    var sourceRoot: Provider<Directory>
    var outputRoot: Provider<Directory>
    var dataRoot: Provider<Directory>
    var sourceFiles: Set<File>
    var sourceEncoding: Provider<String>
    var outputEncoding: Provider<String>
    var caseSensitive: Provider<Boolean>
    var dontTraverseDirectories: Provider<Boolean>
    var alwaysCreateDirectories: Provider<Boolean>
    var removeExtensions: Provider<List<String>>
    var replaceExtensions: Provider<Map<String, String>>
    var removePostfixes: Provider<List<String>>
    var removeFreemarkerExtensions: Provider<Boolean>
    var locale: Provider<String>
    var timeZone: Provider<String>
    var booleanFormat: Provider<String>
    var numberFormat: Provider<String>
    var dateFormat: Provider<String>
    var timeFormat: Provider<String>
    var dateTimeFormat: Provider<String>
    var attributes: Provider<Map<String, Any>>
    var loadDataOperations: Provider<List<LoadDataOperation>>
    var data: Provider<Map<String, Any?>>
}
