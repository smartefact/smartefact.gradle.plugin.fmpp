/*
 * Copyright 2023 Smartefact
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package smartefact.gradle.fmpp

import fmpp.Engine
import java.util.Locale
import org.gradle.api.provider.ListProperty
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property

/**
 * FMPP project extension.
 *
 * @author Laurent Pireyn
 */
abstract class FmppExtension {
    /**
     * FMPP version to use.
     *
     * Default: [FmppPlugin.DEFAULT_FMPP_VERSION]
     */
    abstract val fmppVersion: Property<String>
    init {
        fmppVersion.convention(FmppPlugin.DEFAULT_FMPP_VERSION)
    }

    /**
     * Source encoding.
     *
     * Default: `UTF-8`
     */
    abstract val sourceEncoding: Property<String>
    init {
        sourceEncoding.convention(Charsets.UTF_8.name())
    }

    /**
     * Output encoding.
     *
     * Default: `source`, i.e. same as [sourceEncoding]
     */
    abstract val outputEncoding: Property<String>
    init {
        outputEncoding.convention(Engine.PARAMETER_VALUE_SOURCE)
    }

    abstract val caseSensitive: Property<Boolean>
    init {
        caseSensitive.convention(true)
    }

    abstract val dontTraverseDirectories: Property<Boolean>
    init {
        dontTraverseDirectories.convention(false)
    }

    abstract val alwaysCreateDirectories: Property<Boolean>
    init {
        alwaysCreateDirectories.convention(false)
    }

    abstract val removeExtensions: ListProperty<String>

    abstract val replaceExtensions: MapProperty<String, String>

    abstract val removePostfixes: ListProperty<String>

    abstract val removeFreemarkerExtensions: Property<Boolean>
    init {
        removeFreemarkerExtensions.convention(true)
    }

    /**
     * Locale.
     *
     * Default: `en_US`
     */
    abstract val locale: Property<String>
    init {
        locale.convention(Locale.US.toString())
    }

    abstract val timeZone: Property<String>

    abstract val booleanFormat: Property<String>

    abstract val numberFormat: Property<String>

    abstract val dateFormat: Property<String>

    abstract val timeFormat: Property<String>

    abstract val dateTimeFormat: Property<String>

    /**
     * Attributes.
     *
     * These attributes will be used by all FMPP tasks.
     */
    abstract val attributes: MapProperty<String, Any>

    internal abstract val loadDataOperations: ListProperty<LoadDataOperation>

    /**
     * Loads data under the given [name],
     * using the given [data loader][dataLoaderName] and [args].
     *
     * These data will be used by all FMPP tasks.
     */
    fun data(name: String, dataLoaderName: String, args: List<Any>) {
        loadDataOperations.add(LoadDataOperation(name, dataLoaderName, args))
    }

    /**
     * Loads data under the given [name],
     * using the given [data loader][dataLoaderName] and [args].
     *
     * These data will be used by all FMPP tasks.
     */
    fun data(name: String, dataLoaderName: String, vararg args: Any) {
        data(name, dataLoaderName, args.toList())
    }

    /**
     * Data available to all templates.
     *
     * These data will be used by all FMPP tasks.
     */
    abstract val data: MapProperty<String, Any?>

    // TODO: freemarkerLinks

    // TODO: urlEscapingCharset

    // TODO: outputFormat

    // TODO: outputFormatChooser

    // TODO: mapCommonExtensionsToOutputFormats

    // TODO: modeChooser

    // TODO: headerChooser

    // TODO: footerChooser

    // TODO: turnChooser

    // TODO: XML-related properties

    // TODO: ignoreCsvFiles

    // TODO: ignoreSvnFiles

    // TODO: ignoreTemporaryFiles

    // TODO: skipUnchanged
}
